(function($) {
  var ResourceFilter = {
    topic: '',
    type: '',
    $no_results_msg: [],
    $resources: [],
    init: function() {
      var self = ResourceFilter;
      self.$resources = $('.js-resource-tile');
      if (self.$resources.length > 0) {
        self.setup();
      }
    },
    setup: function() {
      var self = this;
      self.$no_results_msg = $('.js-resources-no-results-msg');
      $('select[name="resource_topic"]').on('change', self.topicChange).select2();
      $('select[name="resource_type"]').on('change', self.typeChange).select2();
    },
    topicChange: function() {
      var self = ResourceFilter;
      self.topic = this.value;
      self.filterResources();
    },
    typeChange: function() {
      var self = ResourceFilter;
      self.type = this.value;
      self.filterResources();
    },
    filterResources: function() {
      var self = this;
      var selector = self.buildFilterSelector();
      self.$no_results_msg.addClass('hidden');
      if (selector === '') {
        // Show All
        self.$resources.removeClass('hidden');
      } else {
        // Show Some
        self.$resources.addClass('hidden');
        if ( self.$resources.filter(selector).removeClass('hidden').length === 0 ) {
          self.$no_results_msg.removeClass('hidden');
        }
      }
    },
    buildFilterSelector: function() {
      var self = this;
      var selector = '';

      if (self.topic) {
        selector += '[data-resource-topics*="||' + self.topic + '||"]';
      }
      if (self.type) {
        selector += '[data-resource-type="' + self.type + '"]';
      }

      return selector;
    }
  };
  $(ResourceFilter.init);
})(jQuery);
